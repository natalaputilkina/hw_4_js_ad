// Теоретичне питання
// Поясніть своїми словами, що таке AJAX і чим він корисний при розробці Javascript.
// Асинхронний JS що дає можливість направляти і отримувати запити без перезавантаження сторінки.

// Завдання
// Отримати список фільмів серії Зоряні війни та вивести на екран список персонажів для кожного з них.

// Технічні вимоги:
// Надіслати AJAX запит на адресу https://ajax.test-danit.com/api/swapi/films та отримати список усіх фільмів серії Зоряні війни
// Для кожного фільму отримати з сервера список персонажів, які були показані у цьому фільмі. Список персонажів можна отримати з властивості characters.
// Як тільки з сервера буде отримана інформація про фільми, відразу вивести список усіх фільмів на екрані. Необхідно вказати номер епізоду, назву фільму, а також короткий зміст (поля episodeId, name, openingCrawl).
// Як тільки з сервера буде отримано інформацію про персонажів будь-якого фільму, вивести цю інформацію на екран під назвою фільму.

const FILMS = "https://ajax.test-danit.com/api/swapi/films";

const listFilms = document.getElementById('list-films');

function updateInfo() {
  fetch(FILMS)
  .then(Response => Response.json())
  .then(data => renderList(data))
}

function renderList (data){
  listFilms.innerHTML = null;
  data.forEach(element => {
    const name = document.createElement('li');
    name.id = element.id;
    name.textContent = element.name;
    const episodeId = document.createElement('p');
    episodeId.textContent = `Episode ${element.episodeId}`;
    const openingCrawl = document.createElement('p');
    openingCrawl.textContent = element.openingCrawl;

    
      const listCharacters = element.characters;
      console.log(listCharacters)
      const a = listCharacters.forEach(character => {
               
        const NAMECHARACTER = character;

        function updateCharacter (){
          fetch(NAMECHARACTER)
          .then(Response => Response.json())
          .then(dataCharacter => renderLictCharacter(dataCharacter))
        }
        
        function renderLictCharacter (dataCharacter){
          const listCharactersFilm = document.createElement('ul');
          listCharactersFilm.innerHTML = null;
          const nameCharacters = document.createElement('li');
          nameCharacters.textContent = dataCharacter.name;
          console.log(nameCharacters)
          listCharactersFilm.append(nameCharacters)
           name.append(listCharactersFilm)

        }
        updateCharacter ()
        
      });

      
    listFilms.append(name);
    name.append(episodeId);
    name.append(openingCrawl);
    
  });
}

updateInfo()